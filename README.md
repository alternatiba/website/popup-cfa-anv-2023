# Pop-up CFA ANV 2023

Code pour la pop-up de la collecte de fin d'année 2023 - ANV-COP21

## Fonctionnement

La pop-up apparaît sur le site web https://anv-cop21.org/
On utilise l'extension "Hustle" : https://anv-cop21.org/wp-admin/admin.php?page=hustle_popup_listing

On crée deux pop-up différentes :
* une pour vue mobile
* une pour vue desktop

## Configuration

Paramètres à configurer :

* Content > Main content : sélectionne la vue "Texte" et copier le code HTML (commun mobile et desktop)
* Appearance > Custom CSS : activer le CSS personnalisé et copier le code CSS (propre à la vue mobile ou la vue desktop)
* Visibility > Visibility Rules :
  * Visitor's Device = Mobile only / Desktop only
  * Pages = All
* Behavior > Schedule : configurer les dates de visiblité
* Behavior > Pop-up Trigger : Scroll 20 %
* Behavior > Animation Settings : Fade in/out pour mobile, Slide in/out left pour desktop
* Behavior > Closing Behavior : quand on clique sur la croix, ne plus montrer la popup pendant un jour
* peut-être d'autres...

## Code CSS dans les fichiers de thème

Ajouter également le code suivant dans le `style.css` dans [l'éditeur de fichiers du thème officiel ANV-COP21](https://anv-cop21.org/wp-admin/theme-editor.php?file=style.css&theme=anv-cop21)

```
/* POPUP HUSTLE */
/* Mise en place par Gauthier le 26/11/2022 pour la campagne de collecte de fin d'année - Utilise le plugin Hustle */
.hustle-popup {
	width:auto !important; /* Pour éviter que la popup ne recouvre la page (width 100vw) et empêche la navigation */
}
/* FIN POPUP HUSTLE */
```

## Voir aussi

https://framagit.org/alternatiba/website/banniere-cfa-anv-2023
